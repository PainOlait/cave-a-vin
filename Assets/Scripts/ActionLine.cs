using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionLine : MonoBehaviour
{
    private CanvasGroup canvasGroup;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void ActivateActionLine()
    {
        canvasGroup.alpha = 1;
        canvasGroup.interactable = true;
    }

    public void DeactivateActionLine()
    {
        canvasGroup.alpha = 0.5f;
        canvasGroup.interactable = false;
    }

}
