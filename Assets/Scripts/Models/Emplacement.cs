using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Emplacement : MonoBehaviour
{
    [SerializeField] private EmplacementWidget emplacementWidget;

    public int index;
    public Bouteille bouteille = null;
    public Image fillColor;

    private void Start()
    {
        emplacementWidget = FindObjectOfType<EmplacementWidget>();
        if (emplacementWidget == null)
            Debug.LogWarning("emplacementWidget pas r�f�renc�");
    }

    public void OnClick()
    {
        emplacementWidget.EmplacementSelection(index);
    }
}
