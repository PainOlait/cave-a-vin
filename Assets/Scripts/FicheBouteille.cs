using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FicheBouteille : MonoBehaviour
{
    public Bouteille bouteille;
    public bool editMode = false;
    private TMP_Text nomBouteille;

    [SerializeField] private GameObject ficheDisplay;
    [SerializeField] private GameObject ficheEdit;


    public void SetDisplayValues()
    {
        if (bouteille != null)
        {
            nomBouteille.text = bouteille.nom;
        }
        else
        {
            Debug.Log("La bouteille n'est pas d�finie");
        }
    }

    public void SetFiche()
    {
        if (editMode)
        {
            ficheEdit.SetActive(true);
            ficheDisplay.SetActive(false);
        }
        else
        {
            ficheEdit.SetActive(false);
            ficheDisplay.SetActive(true);
        }
    }

}
