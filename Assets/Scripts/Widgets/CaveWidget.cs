using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaveWidget : MonoBehaviour
{
    [SerializeField] PopupWidget popup;
    [SerializeField] ActionLine actionLine;
    [SerializeField] EmplacementWidget emplacements;

    public void EmplacementSelected()
    {
        actionLine.ActivateActionLine();
        ShowPopup();
    }

    public void EmplacementDeselected()
    {
        actionLine.DeactivateActionLine();
    }

    private void ShowPopup()
    {
        popup.gameObject.SetActive(true);
        
        popup.SetPopupValue(emplacements.GetEmplacementData());
    }

}
