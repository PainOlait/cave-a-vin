using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DropdownWidget : MonoBehaviour
{
    [SerializeField] private TMP_Dropdown dropdown = null;
    private string selectedValue = "";


    private void OnEnable()
    {
        dropdown.onValueChanged.AddListener(delegate { DropdownValueChanged(dropdown); });
    }

    private void OnDisable()
    {
        dropdown.onValueChanged.RemoveListener(delegate { DropdownValueChanged(dropdown); });
    }
    public void DropdownValueChanged(TMP_Dropdown _dropdown)
    {
        Debug.Log("Dropdown index changed to: " + dropdown.value.ToString());
    }
}
