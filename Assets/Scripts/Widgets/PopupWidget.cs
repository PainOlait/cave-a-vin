using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PopupWidget : MonoBehaviour
{
    [SerializeField] TMP_Text nomEmplacement;

    public void SetPopupValue(Bouteille bouteilleData)
    {
        if (bouteilleData == null)
        {
            nomEmplacement.text = "Emplacement vide";
        }
        else
        {
            nomEmplacement.text = bouteilleData.nom;
        }
    }

}
