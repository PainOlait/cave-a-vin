using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EmplacementWidget : MonoBehaviour
{
    [SerializeField] private CaveWidget caveWidget;

    private List<Emplacement> emplacements;

    [SerializeField] private Color emptyColor;
    [SerializeField] private Color fullColor;
    [SerializeField] private Color selectedColor;

    private bool activeSelection = false;
    private int selectionIndex;

    private void Start()
    {
        foreach (Emplacement e in FindObjectsOfType<Emplacement>())
        {
            Debug.Log(e.gameObject.name);
            emplacements.Add(e);
        }
        RefreshLayout();
        SetEmplacementIndexes();
    }

    private void SetEmplacementIndexes()
    {
        for (int i = 0; i < emplacements.Count; i++)
        {
            emplacements[i].index = i;
        }
    }

    public void RefreshLayout()
    {
        for (int i = 0; i > emplacements.Count; i++)
        {
            SetEmplacementColor(i);
        }
    }

    private void SetEmplacementColor(int _index)
    {
        if (emplacements[_index].bouteille != null)
            emplacements[_index].fillColor.color = fullColor;
        else
            emplacements[_index].fillColor.color = emptyColor;
    }

    public void EmplacementSelection(int _index)
    {
        if (activeSelection)
        {
            EmplacementDeselection();
        }
        activeSelection = true;
        selectionIndex = _index;
        emplacements[selectionIndex].fillColor.color = selectedColor;
        caveWidget.EmplacementSelected();
    }

    public void EmplacementDeselection()
    {
        SetEmplacementColor(selectionIndex);
        caveWidget.EmplacementDeselected();
    }

    public Bouteille GetEmplacementData()
    {
        if (activeSelection)
        {
            return emplacements[selectionIndex].bouteille;
        }
        else
        {
            return null;
        }
    }
}
